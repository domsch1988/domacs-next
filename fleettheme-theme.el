(deftheme fleettheme
  "Created 2023-11-23.")

(defgroup fleettheme nil
  "Zeno-theme options."
  :group 'faces)

(defcustom fleettheme-enable-italics t
  "Enable italics for functions, comments, directories."
  :type 'boolean
  :group 'fleettheme)

(defcustom fleettheme-enable-bold t
  "Enable italics for functions, comments, directories."
  :type 'boolean
  :group 'fleettheme)

(defcustom fleettheme-highlight-color "#7fd6d6"
  "Set a highlight color"
  :type 'string
  :group 'fleettheme)

(defcustom fleettheme-enable-monaspace-variants t
  "Enable Different Fonts through the Monaspace Font Variants"
  :type 'boolean
  :group 'fleettheme)

(let
    (
     (bg                     "#0D0D0D") ;; Base Background
     (bg-1                   "#1C1C1C") ;; Lighter
     (bg-2                   "#212121") ;; Lighter
     (bg-border              "#383838") ;; Lighter
     (fg                     "#E2E2E2")
     (fg-gray                "#C2C2C2")
     (fg-dim                 "#5D5D5D")
     (fg-red                 "#dd705a")
     (fg-green               "#b7de87")
     (fg-yellow              "#f7d18f")
     (fg-blue                "#7dbeff")
     (fg-magenta             "#ad99ff")
     (fg-pink                "#f095e8")
     (fg-cyan                "#79c1be")
     
     (slantType (if fleettheme-enable-italics 'italic 'normal))
     (weightType (if fleettheme-enable-bold 'bold 'normal))

   )

(custom-theme-set-faces
 'fleettheme

;; Default and Interface
`(default ((t (:background ,bg :foreground ,fg))))
`(internal-border ((t (:background ,bg :foreground ,bg-border))))
`(vertical-border ((t (:background ,bg :foreground ,bg-border))))
`(child-frame-border ((t (:background ,bg :foreground ,bg-border))))
`(error ((t VjV(:background ,bg :foreground ,fg-red))))
`(hl-line ((t (:background ,bg-1))))
`(rectangle-preview ((t (:background ,bg-1))))
`(region ((t (:background ,bg-1))))
`(line-number ((t (:foreground ,fg-dim))))
`(line-number-current-line ((t (:background ,bg-1 :foreground fg-gray))))
`(highlight ((t (:background ,bg-1))))
`(minibuffer-prompt ((t (:foreground ,fg-blue))))


;; general Highlighting
`(font-lock-constant-face ((t (:foreground ,fg))))
`(font-lock-type-face ((t (:foreground ,fg-yellow))))
`(font-lock-comment-face ((t (:foreground ,fg-dim :slant ,slantType))))
`(font-lock-string-face ((t (:foreground ,fg-pink :slant ,slantType))))
`(font-lock-builtin-face ((t (:foreground ,fg-blue :weight ,weightType))))
`(font-lock-keyword-face ((t (:foreground ,fg-cyan :weight ,weightType))))
`(font-lock-variable-use-face ((t (:foreground ,fg :slant ,slantType))))
`(font-lock-function-use-face ((t (:foreground ,fg :slant ,slantType))))
`(font-lock-variable-name-face ((t (:foreground ,fg :slant ,slantType))))
`(font-lock-function-name-face ((t (:foreground ,fg :slant ,slantType))))

`(show-paren-match ((t (:background ,fg-blue :foreground ,bg :weight ,weightType))))

;; Orderless
`(orderless-match-face-0 ((t (:foreground ,fg-blue :weight ,weightType))))
`(orderless-match-face-1 ((t (:foreground ,fg-pink :weight ,weightType))))
`(orderless-match-face-2 ((t (:foreground ,fg-magenta :weight ,weightType :slant ,slantType))))
`(orderless-match-face-3 ((t (:foreground ,fg-cyan :weight ,weightType :slant ,slantType))))

;; Modeline
`(telephone-line-evil ((t (:background ,bg-1))))
`(telephone-line-evil-normal ((t (:background ,fg-blue :foreground ,bg))))
`(telephone-line-evil-insert ((t (:background ,fg-green :foreground ,bg))))
`(telephone-line-evil-emacs ((t (:background ,fg-red :foreground ,bg))))
`(mode-line ((t (:background ,bg-1 :foreground ,fg))))

;; Magit
`(magit-branch-local ((t (:foreground ,fg :weight ,weightType :slant ,slantType))))
`(magit-branch-remote ((t (:foreground ,fg :weight ,weightType :slant ,slantType))))
`(magit-section-highlight ((t (:background ,bg-1))))
`(magit-header-line ((t (:foreground ,fg :weight ,weightType))))
`(magit-section-heading ((t (:foreground ,fg :weight ,weightType))))

;; Centaur Tabs
`(tab-bar ((t (:background ,bg-2))))
`(tab-line ((t (:background ,bg-2))))
`(centaur-tabs-selected ((t (:background ,bg :foreground ,fg :weight ,weightType))))
`(centaur-tabs-unselected ((t (:background ,bg-2 :foreground ,fg))))
`(centaur-tabs-active-bar-face ((t (:foreground ,fg))))
`(centaur-tabs-selected-modified ((t (:background ,bg :foreground ,fg-red :weight ,weightType))))
`(centaur-tabs-unselected-modified ((t (:background ,bg-2 :foreground ,fg-red))))

;; Treemacs
`(treemacs-directory-face ((t (:foreground ,fg))))
`(treemacs-all-the-icons-file-face ((t (:foreground ,fg))))
`(treemacs-all-the-icons-directory-face ((t (:foreground ,fg))))
`(treemacs-root-face ((t (:foreground ,fg :underline nil :weight ,weightType))))

))

;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'fleettheme)
