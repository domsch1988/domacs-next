;; -*- mode: emacs-lisp; lexical-binding:t; coding: utf-8-emacs; -*-
;; -------------------------------------------------------------------------
;; Tabspaces Session File for Emacs
;; -------------------------------------------------------------------------
;; Created Wed Nov 22 12:03:17 2023

;; Tabs and buffers:
(setq tabspaces--session-list '((nil . "*dashboard*") (("d:/.config/emacs/init.el") . "Emacs") (("d:/.config/emacs/init.el") . "Ansible")))