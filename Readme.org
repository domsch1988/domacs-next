#+PROPERTY: header-args :tangle init.el :mkdirp yes

* Ideas
Here we want to keep a running list of configs/Features we might look into

** TODO Org Templates
Setting up Templates for Reoccuring Checklists (Updates or such)

** TODO One Orgconfig to rule them all?
Maybe we want to get our entire system Configuration tangled from one
Org-File? Include Neovim and Nix Stuff in this Place as well?

* Elpaca Setup
#+begin_src emacs-lisp
  (defvar elpaca-installer-version 0.6)
  (defvar elpaca-directory (expand-file-name "elpaca/" user-emacs-directory))
  (defvar elpaca-builds-directory (expand-file-name "builds/" elpaca-directory))
  (defvar elpaca-repos-directory (expand-file-name "repos/" elpaca-directory))
  (defvar elpaca-order '(elpaca :repo "https://github.com/progfolio/elpaca.git"
				:ref nil
				:files (:defaults "elpaca-test.el" (:exclude "extensions"))
				:build (:not elpaca--activate-package)))
  (let* ((repo  (expand-file-name "elpaca/" elpaca-repos-directory))
	 (build (expand-file-name "elpaca/" elpaca-builds-directory))
	 (order (cdr elpaca-order))
	 (default-directory repo))
    (add-to-list 'load-path (if (file-exists-p build) build repo))
    (unless (file-exists-p repo)
      (make-directory repo t)
      (when (< emacs-major-version 28) (require 'subr-x))
      (condition-case-unless-debug err
	  (if-let ((buffer (pop-to-buffer-same-window "*elpaca-bootstrap*"))
		   ((zerop (call-process "git" nil buffer t "clone"
					 (plist-get order :repo) repo)))
		   ((zerop (call-process "git" nil buffer t "checkout"
					 (or (plist-get order :ref) "--"))))
		   (emacs (concat invocation-directory invocation-name))
		   ((zerop (call-process emacs nil buffer nil "-Q" "-L" "." "--batch"
					 "--eval" "(byte-recompile-directory \".\" 0 'force)")))
		   ((require 'elpaca))
		   ((elpaca-generate-autoloads "elpaca" repo)))
	      (progn (message "%s" (buffer-string)) (kill-buffer buffer))
	    (error "%s" (with-current-buffer buffer (buffer-string))))
	((error) (warn "%s" err) (delete-directory repo 'recursive))))
    (unless (require 'elpaca-autoloads nil t)
      (require 'elpaca)
      (elpaca-generate-autoloads "elpaca" repo)
      (load "./elpaca-autoloads")))
  (add-hook 'after-init-hook #'elpaca-process-queues)
  (elpaca `(,@elpaca-order))

  (elpaca elpaca-use-package
    (elpaca-use-package-mode)
    (setq elpaca-use-package-by-default t))
  (elpaca-wait)
#+end_src

* Variable and Function Definitions
#+begin_src emacs-lisp
  (defvar dom/font-fixed "Fira Code")
  (defvar dom/size-fixed 120)
  (defvar dom/font-variable "InputSansCondensed")
  (defvar dom/size-variable 120)

  (defun dom-indent-buffer ()
    (interactive)
    (save-excursion
      (indent-region (point-min) (point-max) nil)))

  (defun dom-copy-line (arg)
    "Copy lines (as many as prefix argument) in the kill ring"
    (interactive "p")
    (kill-ring-save (line-beginning-position)
		    (line-beginning-position (+ 1 arg)))
    (message "%d line%s copied" arg (if (= 1 arg) "" "s")))
#+end_src

* WSL Fixes
This should allow Copying from WSL EMacs to Windows Applications
https://github.com/microsoft/wslg/issues/15#issuecomment-1193370697
#+begin_src emacs-lisp
  (when (and (getenv "WAYLAND_DISPLAY") (not (equal (getenv "GDK_BACKEND") "x11")))
    (setq
     interprogram-cut-function
     (lambda (text)
       ;; strangest thing: gui-select-text leads to gui-set-selection 'CLIPBOARD
       ;; text -- if I eval that with some string, it mostly lands on the wayland
       ;; clipboard, but not when it's invoked from this context.
       ;; (gui-set-selection 'CLIPBOARD text)
       ;; without the charset=utf-8 in type, emacs / wl-copy will crash when you paste emojis into a windows app
       (start-process "wl-copy" nil "wl-copy" "--trim-newline" "--type" "text/plain;charset=utf-8"  text))))
#+end_src

* Mail?

#+begin_src emacs-lisp
    (add-to-list 'load-path "/usr/share/emacs/site-lisp/mu4e")

    (use-package mu4e
      :elpaca nil)
#+end_src

* Emacs Base Setup
Use package is used with skipping elpaca to make Settings in emacs.

#+begin_src emacs-lisp
  (use-package emacs
    :elpaca nil
    :init
    (define-prefix-command 'dom-map)
    (defun crm-indicator (args)
      (cons (format "[CRM%s] %s"
		    (replace-regexp-in-string
		     "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
		     crm-separator)
		    (car args))
	    (cdr args)))
    (advice-add #'completing-read-multiple :filter-args #'crm-indicator)
    :hook
    (emacs-lisp-mode-hook . hs-minor-mode)
    (minibuffer-setup-hook . cursor-intangible-mode)
    :config
    (menu-bar-mode -1)
    (scroll-bar-mode -1)
    (tool-bar-mode -1)
    (display-time-mode 1)
    (recentf-mode 1)
    (save-place-mode 1)
    (savehist-mode 1)
    (visual-line-mode 1)
    (desktop-save-mode 1)
    (add-to-list 'load-path "/usr/share/emacs/site-lisp")
    :custom
    (ring-bell-function #'ignore)
    (display-line-numbers-type 'relative)
    (inhibit-startup-screen t)
    (split-height-threshold 100)
    (split-width-threshold 100)
    (desktop-save t)
    (desktop-save-buffer t)
    (make-backup-files nil)
    (create-lockfiles nil)
    (enable-recursive-minibuffers t)
    (minibuffer-prompt-properties
     '(read-only t cursor-intangible t face minibuffer-prompt))
    (auto-save-file-name-transforms '((".*" "~/.config/emacs/autosaves/\\1" t)))
    (backup-directory-alist '((".*" . "~/.config/emacs/backups/")))
    (native-comp-async-report-warnings-errors nil)
    (fringe-indicator-alist '((truncation nil nil)
			      (continuation nil nil)
			      (overlay-arrow . right-triangle) (up . up-arrow) (down . down-arrow)
			      (top top-left-angle top-right-angle)
			      (bottom bottom-left-angle bottom-right-angle top-right-angle
				      top-left-angle)
			      (top-bottom left-bracket right-bracket top-right-angle top-left-angle)
			      (empty-line . empty-line) (unknown . question-mark)))
    :custom-face
    (default ((t (:family ,dom/font-fixed :height ,dom/size-fixed))))
    (fixed-pitch ((t (:font ,dom/font-fixed :height ,dom/size-fixed))))
    (variable-pitch ((t (:font ,dom/font-variable :height ,dom/size-variable))))
    (mode-line ((t (:background nil :overline nil :box nil))))
    (mode-line-active ((t (:background nil :overline (:color "black" :position t) :box nil))))
    (mode-line-inactive ((t (:background nil :overline (:color "black" :position t) :box nil))))
    (header-line ((t (:background nil :underline (:color "black" :position t) :box nil :height 140))))
    :bind-keymap
    ("C-d" . dom-map)
    :bind
    (("<escape>" . keyboard-escape-quit)
     ("C-M-S-l" . windmove-right)
     ("C-M-S-j" . windmove-down)
     ("C-M-S-k" . windmove-up)
     ("C-M-S-h" . windmove-left)
     :map dom-map
     ("u s" . spacious-padding-mode)
     ("u l" . display-line-numbers-mode)
     ("u m" . dom-minimize-modeline)
     ("b f" . dom-indent-buffer)
     ("y y" . dom-copy-line)
     ("d l" . duplicate-line)
     )
    )
#+end_src

* Utility Stuff
Adding a bunch of Utility packages, that get used by other packages later
#+begin_src emacs-lisp
  (use-package all-the-icons)

  (use-package nerd-icons
    :demand t)

#+end_src

Esup is used for profiling the configuration
#+begin_src emacs-lisp
  (use-package esup
    :demand t
    :custom
    (esup-depth 0)
    )
#+end_src

** Mode Customization
This cleans up our Modeline. Diminish provides a Keyword in use-package as well.

#+begin_src emacs-lisp
  (use-package diminish
    :demand t)
#+end_src

Delight allows replacing Modes Names

#+begin_src emacs-lisp
  (use-package delight
    :demand t)
#+end_src

* Evil Setup
I think this is the first Major Block. We want this loaded before anything else

#+begin_src emacs-lisp
  ;; (use-package evil
  ;;   :demand t
  ;;   :config
  ;;   (evil-mode 1)
  ;;   :hook
  ;;   (dashboard-mode . turn-off-evil-mode)
  ;;   (dired-mode . turn-off-evil-mode)
  ;;   )

  ;; (use-package evil-nerd-commenter)

  ;; (use-package evil-collection)

  ;; (use-package evil-leader
  ;;   :demand t
  ;;   :config
  ;;   (global-evil-leader-mode)
  ;;   (evil-leader/set-leader "<SPC>")
  ;;   (evil-leader/set-key
  ;;     "f f" 'project-find-file
  ;;     "f g" 'consult-ripgrep
  ;;     "g g" 'magit
  ;;     "o t" 'org-babel-tangle
  ;;     "u e" 'treemacs
  ;;     "s s" 'project-switch-project
  ;;     "b b" 'project-switch-to-buffer
  ;;     "s o" 'desktop-read
  ;;     "s w" 'desktop-save-in-desktop-dir
  ;;     "q q" 'kill-emacs
  ;;     "b b" 'switch-to-buffer
  ;;     "b f" 'indent-buffer
  ;;     "k b" 'kill-buffer
  ;;     "y s" 'company-yasnippet
  ;;     "y i" 'yas-insert-snippet
  ;;     "w w" 'ace-window
  ;;     "w c" 'delete-window
  ;;     "r f" 'org-roam-node-find
  ;;     "r l" 'org-roam-node-insert
  ;;     "w e" '(lambda () (interactive)(evil-window-set-width 180))
  ;;     "w j" '(lambda () (interactive)(split-window-vertically) (other-window 1))
  ;;     "w l" '(lambda () (interactive)(split-window-horizontally) (other-window 1))
  ;;     "k k" 'kill-buffer-and-window)
  ;;   )
#+end_src

* Org-Mode
Everything regarding ORG-Mode

#+begin_src emacs-lisp
  (use-package org
    :elpaca nil
    :demand t
    :custom
    (org-blank-before-new-entry '((heading . t) (plain-list-item . nil)))
    (org-timer-default-timer 60)

    (modus-themes-headings
     '((1 bold variable-pitch 1.5)
       (2 bold variable-pitch 1.3)
       (3 bold variable-pitch 1.2)
       (agenda-date 1.3)
       (agenda-structure variable-pitch light 1.8)
       (t variable-pitch)))
    (org-capture-templates
     '(("t" "Todo")
       ("tt" "General" entry (file+headline "~/org-notes/todo.org" "Tasks")
	"* TODO %?\n  %i")
       ("tw" "Work" entry (file+headline "~/org-notes/todo-work.org" "Tasks")
	"* TODO %?\n  %i")
       ("te" "Emacs" entry (file+headline "~/org-notes/todo-emacs.org" "Tasks")
	"* TODO %?\n  %i")
       ("j" "Journal" entry (file+datetree "~/org/notes.org")
	"* %?\nEntered on %U\n  %i")))
    :hook
    (org-mode . visual-line-mode)
    (org-mode . prettify-symbols-mode)
    (org-mode . (lambda () (setq prettify-symbols-alist '(
							  ("TODO" . ?)
							  ("DONE" . ?)
							  ))))
    :custom-face
    (org-block ((nil (:inherit 'fixed-pitch))))
    (org-document-title ((t (:height 200))))
    :bind
    (:map dom-map
	  ("o c" . org-capture)
	  ("o t" . org-timer-set-timer)
	  )
    )

  (use-package org-roam
    :demand t
    :custom
    (org-roam-directory (file-truename "~/org-notes/org-roam"))
    :config
    (org-roam-db-autosync-mode)
    :bind
    (:map dom-map
	  ("r f" . org-roam-node-find)
	  ("r i" . org-roam-node-insert)
	  ("r s" . org-roam-buffer-toggle)
	  )
    )

  (use-package org-modern
    :demand t
    :custom
    (org-modern-block-fringe nil)
    (org-modern-todo nil)
    (org-modern-star '(" " "   " "     " "       " "         "))
    :hook
    (org-mode . org-modern-mode)
    )

#+end_src

* UI Plugins
Here we have various UI "enhancements

** Visual Fill Column
This Centers our Working Area

#+begin_src emacs-lisp
  (use-package visual-fill-column
    :demand t
    :custom
    (visual-fill-column-center-text t)
    (visual-fill-column-width 140)
    :init
    (global-visual-fill-column-mode)
    )
#+end_src

** Prot Plugins for Testing

#+begin_src emacs-lisp
  (use-package spacious-padding
    :demand t
    :after modus-themes
    :config
    (spacious-padding-mode 1)
    )

  (use-package substitute
    :demand t)

  (use-package lin
    :demand t
    :config
    (setq lin-mode-hooks
	  '(bongo-mode-hook
	    dired-mode-hook
	    elfeed-search-mode-hook
	    git-rebase-mode-hook
	    grep-mode-hook
	    ibuffer-mode-hook
	    ilist-mode-hook
	    ledger-report-mode-hook
	    log-view-mode-hook
	    magit-log-mode-hook
	    mu4e-headers-mode-hook
	    notmuch-search-mode-hook
	    notmuch-tree-mode-hook
	    occur-mode-hook
	    org-agenda-mode-hook
	    pdf-outline-buffer-mode-hook
	    proced-mode-hook
	    tabulated-list-mode-hook))

    (lin-global-mode 1))

  (use-package notmuch
    :demand t)
#+end_src
** Ligatures
#+begin_src emacs-lisp
  (use-package ligature
    :config
    ;; Enable the "www" ligature in every possible major mode
    (ligature-set-ligatures 't '("www"))
    ;; Enable traditional ligature support in eww-mode, if the
    ;; `variable-pitch' face supports it
    (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
    ;; Enable all Cascadia Code ligatures in programming modes
    (ligature-set-ligatures 't '("|||>" "<|||" "<==>" "<!--" "####" "~~>" "***" "||=" "||>"
				 ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
				 "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
				 "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
				 "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
				 "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
				 "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
				 "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" "!!" ">:"
				 ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|" "<:"
				 "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
				 "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
				 "?=" "?." "??" ";;" "/*" "/=" "/>" "//" "__" "~~" "(*" "*)"
				 "\\\\" "://"))
    ;; Enables ligature checks globally in all buffers. You can also do it
    ;; per mode with `ligature-mode'.
    (global-ligature-mode t))


#+end_src
** Modeline

Two Part Modeline:

This is the Bottom Part:

#+begin_src emacs-lisp

  (setq display-time-format "%H:%M")
  ;; (setq org-timer-display nil)

  (defvar-local dom-line-buffer-name
      '(:eval
	(list
	 " "
	 (if (mode-line-window-selected-p)
	     (propertize (format "%s" (buffer-name)) 'face 'bold)
	   (propertize (format "%s" (buffer-name)) 'face 'shadow)))
	)
    "My Personal Modelines Buffername")

  (defun dom-number-in-range-p (number min-value max-value)
    "Check if NUMBER is within the range [MIN-VALUE, MAX-VALUE]."
    (and (<= min-value number)
	 (<= number max-value)))

  (defun dom-string-to-minutes (timer-string)
    "Converts an Org mode timer string to minutes.
  TIMER-STRING should be in the format HH:MM:SS."
    (let* ((components (mapcar #'string-to-number (split-string timer-string ":")))
	   (hours (or (nth 0 components) 0))
	   (minutes (or (nth 1 components) 0))
	   (seconds (or (nth 2 components) 0)))
      (+ (* hours 60) minutes (/ seconds 60.0))))

  (defun dom-line-timer ()
      (string-match org-timer-re org-timer-mode-line-string)
      (if (string= org-timer-mode-line-string " <0:00:01>")
	  (" ")
	(match-string 0 org-timer-mode-line-string))
    )


  (defun dom-line-progress ()
    (let ((bar (cond
		(( > (dom-string-to-minutes (dom-line-timer)) 90 ) "  ")
		((dom-number-in-range-p (dom-string-to-minutes (dom-line-timer)) 80 90 ) "  ")
		((dom-number-in-range-p (dom-string-to-minutes (dom-line-timer)) 70 80 ) "  ")
		((dom-number-in-range-p (dom-string-to-minutes (dom-line-timer)) 60 70 ) "  ")
		((dom-number-in-range-p (dom-string-to-minutes (dom-line-timer)) 50 60 ) "  ")
		((dom-number-in-range-p (dom-string-to-minutes (dom-line-timer)) 40 50 ) "  ")
		((dom-number-in-range-p (dom-string-to-minutes (dom-line-timer)) 30 40 ) "  ")
		((dom-number-in-range-p (dom-string-to-minutes (dom-line-timer)) 20 30 ) "  ")
		((dom-number-in-range-p (dom-string-to-minutes (dom-line-timer)) 10 20 ) "  ")
		((dom-number-in-range-p (dom-string-to-minutes (dom-line-timer)) 0 10 )   "  ")
		(t "?"))))
      (propertize bar 'face 'bold)
      )
    )

    (defun my-modeline--major-mode-name ()
      "Return capitalized `major-mode' as a string."
      (capitalize (symbol-name major-mode)))

    (defun dom-line-major-mode-indicator ()
      "Return appropriate propertized mode line indicator for the major mode."
      (let ((indicator (cond
			((string= major-mode "org-mode") "  ")
			((string= major-mode "emacs-lisp-mode") "  ")
			((string= major-mode "lisp-interaction-mode") "  ")
			((string= major-mode "messages-buffer-mode") "  ")
			((string= major-mode "term-mode") " >_ ")
			((string= major-mode "yaml-mode") "  ")
			((string= major-mode "magit-mode") "  ")
			((string= major-mode "magit-status-mode") "  ")
			(t "?"))))
	(propertize indicator 'face 'bold)))

    (defvar-local dom-line-timer-render
	'(:eval
	  (list
	   ""
	   (if (mode-line-window-selected-p)
	   (dom-line-progress)
	   )
	  ))
      "My Renderer for Org Timers"
      )

    (defvar-local dom-line-major-mode-symbol
	'(:eval
	  (list
	   (propertize (dom-line-major-mode-indicator) 'face 'bold)
	   ))
      "Mode line construct to display the major mode.")

    (defvar-local dom-line-major-mode
	'(:eval
	  (list
	   (propertize (my-modeline--major-mode-name) 'face 'bold)
	   ))
      "Mode line construct to display the major mode.")

    (defvar-local dom-git-indicator
      '(:eval
	(list
	 (propertize " " 'face 'error)
	 (string-replace "Git:" "" (vc-git-mode-line-string buffer-file-name))
	 )
	)
    )

  (defface dom-magit-modified-face
  '((t :foreground "red" :inherit bold :height 160))
  "Face with a red background for use on the mode line.")

    (defface dom-magit-pull-face
  '((t :foreground "yellow" :inherit bold :height 160))
  "Face with a red background for use on the mode line.")

    (defface dom-magit-icon-face
  '((t :foreground "black" :inherit bold :height 160))
  "Face with a red background for use on the mode line.")

  (defvar-local dom-magit-indicator
      '(:eval
	(list
	 " "
	 (propertize "  " 'face 'dom-magit-icon-face)
	 (when (magit-git-repo-p (locate-dominating-file buffer-file-name ".git"))
	 (magit-main-branch))
	 (when (magit-anything-modified-p) (propertize "  " 'face 'dom-magit-modified-face))
	 (when (magit-anything-unmerged-p) (propertize "  " 'face 'dom-magit-modified-face))
	 )
	)
    )

    (dolist (construct '( dom-line-buffer-name
			  dom-line-timer-render
			  dom-line-major-mode
			  dom-line-major-mode-symbol
			  dom-git-indicator
			  dom-magit-indicator
			  ))
      (put construct 'risky-local-variable t))

    (setq-default mode-line-format
		  '(
		    "%e"
		    " "
		    dom-line-major-mode
		    mode-line-format-right-align
		    dom-line-timer-render
		    mode-line-misc-info
		    ))


#+end_src

This is the Top Part (Headerline)
#+begin_src emacs-lisp
  (setq mode-line-right-align-edge 'window)
  (setq display-time-load-average-threshold 99)
  (setq-default header-line-format
		'(
		  " "
		  dom-line-major-mode-symbol
		  dom-line-buffer-name
		  "  "
		  dom-magit-indicator
		  )
		)
#+end_src

** Tabs
Centaur Tabs delivers our Tabs. With the Setup below, Tabs get "grouped" by Project. Alternatively grouping can be disabled, to always show all tabs 

#+begin_src emacs-lisp
  (use-package centaur-tabs
    :demand t
    :init
    ;; This separates Tabs by Project. Leave it out to always show all Tabs.
    ;;(centaur-tabs-group-by-projectile-project)
    :custom
    (centaur-tabs-style "bar")
    (centaur-tabs-set-bar 'over)
    (centaur-tabs-height 30)
    (centaur-tabs-close-button " X ")
    (centaur-tabs-set-modified-marker t)
    (centaur-tabs-modified-marker " * ")
    (centaur-tabs-set-icons t)
    (centaur-tabs-icon-type 'default)
    :config
    (defun centaur-tabs-buffer-groups ()
      (list
       (cond
	((string-equal "*" (substring (buffer-name) 0 1)) "Emacs")
	((string-equal "*terminal*" (buffer-name)) "GROUP")
	(t "GROUP"))))
    :bind
    ("C-M-H" . centaur-tabs-backward)
    ("C-M-L" . centaur-tabs-forward))
#+end_src

* Rest
This is the "Everything else" Block. It startet with all the Code and keeps what hasn't been "compartementalized" yet.
#+begin_src emacs-lisp


  (use-package dirvish
    :custom
    (dirvish-attributes '(all-the-icons file-size))
    (dired-listing-switches "-l --almost-all --human-readable --group-directories-first --no-group")
    :config
    (dirvish-override-dired-mode)
    (dirvish-side-follow-mode)
    :hook
    (dired-mode . lin-mode)
    :bind-keymap
    ("C-d" . dom-map)
    :bind
    (
     :map dom-map
     ("C-d" . dirvish-side)
     )
    )

  (use-package yasnippet
    :demand t
    :diminish (yas-minor-mode)
    :config
    (yas-global-mode))

  (use-package yasnippet-snippets
    :after yasnippet
    :demand t)

  (use-package which-key
    :diminish which-key-mode
    :init
    (which-key-mode))

  (use-package vertico
    :init
    (savehist-mode)
    (vertico-mode))

  (use-package orderless
    :custom
    (completion-styles '(orderless basic))
    (completion-category-defaults nil)
    (completion-category-overrides '((file (styles partial-completion)))))

  (use-package consult
    ;; Replace bindings. Lazily loaded due by `use-package'.
    :bind (;; C-c bindings in `mode-specific-map'
	   ("C-c M-x" . consult-mode-command)
	   ("C-c h" . consult-history)
	   ("C-c k" . consult-kmacro)
	   ("C-c m" . consult-man)
	   ("C-c i" . consult-info)
	   ([remap Info-search] . consult-info)
	   ;; C-x bindings in `ctl-x-map'
	   ("C-x M-:" . consult-complex-command)     ;; orig. repeat-complex-command
	   ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
	   ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
	   ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
	   ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
	   ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
	   ;; Custom M-# bindings for fast register access
	   ("M-#" . consult-register-load)
	   ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
	   ("C-M-#" . consult-register)
	   ;; Other custom bindings
	   ("M-y" . consult-yank-pop)                ;; orig. yank-pop
	   ;; M-g bindings in `goto-map'
	   ("M-g e" . consult-compile-error)
	   ("M-g f" . consult-flymake)               ;; Alternative: consult-flycheck
	   ("M-g g" . consult-goto-line)             ;; orig. goto-line
	   ("M-g M-g" . consult-goto-line)           ;; orig. goto-line
	   ("M-g o" . consult-outline)               ;; Alternative: consult-org-heading
	   ("M-g m" . consult-mark)
	   ("M-g k" . consult-global-mark)
	   ("M-g i" . consult-imenu)
	   ("M-g I" . consult-imenu-multi)
	   ;; M-s bindings in `search-map'
	   ("M-s d" . consult-find)
	   ("M-s D" . consult-locate)
	   ("M-s g" . consult-grep)
	   ("M-s G" . consult-git-grep)
	   ("M-s r" . consult-ripgrep)
	   ("M-s l" . consult-line)
	   ("M-s L" . consult-line-multi)
	   ("M-s k" . consult-keep-lines)
	   ("M-s u" . consult-focus-lines)
	   ;; Isearch integration
	   ("M-s e" . consult-isearch-history)
	   :map isearch-mode-map
	   ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
	   ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
	   ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
	   ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
	   ;; Minibuffer history
	   :map minibuffer-local-map
	   ("M-s" . consult-history)                 ;; orig. next-matching-history-element
	   ("M-r" . consult-history))                ;; orig. previous-matching-history-element

    ;; Enable automatic preview at point in the *Completions* buffer. This is
    ;; relevant when you use the default completion UI.
    :hook (completion-list-mode . consult-preview-at-point-mode)
    :init

    ;; Optionally configure the register formatting. This improves the register
    ;; preview for `consult-register', `consult-register-load',
    ;; `consult-register-store' and the Emacs built-ins.
    (setq register-preview-delay 0.5
	  register-preview-function #'consult-register-format)

    ;; Optionally tweak the register preview window.
    ;; This adds thin lines, sorting and hides the mode line of the window.
    (advice-add #'register-preview :override #'consult-register-window)

    ;; Use Consult to select xref locations with preview
    (setq xref-show-xrefs-function #'consult-xref
	  xref-show-definitions-function #'consult-xref)

    ;; Configure other variables and modes in the :config section,
    ;; after lazily loading the package.
    :config

    ;; Optionally configure preview. The default value
    ;; is 'any, such that any key triggers the preview.
    ;; (setq consult-preview-key 'any)
    ;; (setq consult-preview-key "M-.")
    ;; (setq consult-preview-key '("S-<down>" "S-<up>"))
    ;; For some commands and buffer sources it is useful to configure the
    ;; :preview-key on a per-command basis using the `consult-customize' macro.
    (consult-customize
     consult-theme :preview-key '(:debounce 0.2 any)
     consult-ripgrep consult-git-grep consult-grep
     consult-bookmark consult-recent-file consult-xref
     consult--source-bookmark consult--source-file-register
     consult--source-recent-file consult--source-project-recent-file
     ;; :preview-key "M-."
     :preview-key '(:debounce 0.4 any))

    ;; Optionally configure the narrowing key.
    ;; Both < and C-+ work reasonably well.
    (setq consult-narrow-key "<") ;; "C-+"

    ;; Optionally make narrowing help available in the minibuffer.
    ;; You may want to use `embark-prefix-help-command' or which-key instead.
    ;; (define-key consult-narrow-map (vconcat consult-narrow-key "?") #'consult-narrow-help)

    ;; By default `consult-project-function' uses `project-root' from project.el.
    ;; Optionally configure a different project root function.
	  ;;;; 1. project.el (the default)
    ;; (setq consult-project-function #'consult--default-project--function)
	  ;;;; 2. vc.el (vc-root-dir)
    ;; (setq consult-project-function (lambda (_) (vc-root-dir)))
	  ;;;; 3. locate-dominating-file
    ;; (setq consult-project-function (lambda (_) (locate-dominating-file "." ".git")))
	  ;;;; 4. projectile.el (projectile-project-root)
    ;; (autoload 'projectile-project-root "projectile")
    ;; (setq consult-project-function (lambda (_) (projectile-project-root)))
	  ;;;; 5. No project support
    ;; (setq consult-project-function nil)
    )

  (use-package marginalia
    :bind (:map minibuffer-local-map
		("M-A" . marginalia-cycle))
    :init
    (marginalia-mode))

  (use-package embark
    :ensure t

    :bind
    (("C-." . embark-act)         ;; pick some comfortable binding
     ("C-;" . embark-dwim)        ;; good alternative: M-.
     ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

    :init

    ;; Optionally replace the key help with a completing-read interface
    (setq prefix-help-command #'embark-prefix-help-command)

    ;; Show the Embark target at point via Eldoc.  You may adjust the Eldoc
    ;; strategy, if you want to see the documentation from multiple providers.
    (add-hook 'eldoc-documentation-functions #'embark-eldoc-first-target)
    ;; (setq eldoc-documentation-strategy #'eldoc-documentation-compose-eagerly)

    :config

    ;; Hide the mode line of the Embark live/completions buffers
    (add-to-list 'display-buffer-alist
		 '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
		   nil
		   (window-parameters (mode-line-format . none)))))

  (use-package embark-consult
    :ensure t ; only need to install it, embark loads it after consult if found
    :hook
    (embark-collect-mode . consult-preview-at-point-mode))

  (use-package company
    :init
    (global-company-mode)
    :diminish (company-mode))

  (use-package magit
    :defer t
    :bind-keymap
    ("C-d" . dom-map)
    :bind (
	   :map dom-map
	   ("g g" . magit)
	   ("g l" . magit-log)
	   ("g b" . magit-blame)
	   )
    ;; :custom
    ;; (magit-git-executable "C:/Users/dosa/AppData/Local/Programs/Git/cmd/git.exe")
    )

  (use-package keychain-environment
    :demand t)

  ;; (use-package treemacs
  ;;   :demand t
  ;;   :custom-face
  ;;   (treemacs-file-face  ((t (:family ,dom/font-variable))))
  ;;   (treemacs-directory-face  ((t (:family ,dom/font-variable))))
  ;;   (treemacs-git-modified-face  ((t (:family ,dom/font-variable))))
  ;;   (treemacs-git-untracked-face  ((t (:family ,dom/font-variable))))
  ;;   (treemacs-git-ignored-face  ((t (:family ,dom/font-variable))))
  ;;   (treemacs-root-face  ((t (:family ,dom/font-variable))))
  ;;   :hook
  ;;   (treemacs-mode . (lambda () (setq mode-line-format nil)))
  ;;   :config
  ;;   (progn
  ;;     (setq treemacs-collapse-dirs                   (if treemacs-python-executable 3 0)
  ;; 	  treemacs-deferred-git-apply-delay        0.5
  ;; 	  treemacs-directory-name-transformer      #'identity
  ;; 	  treemacs-display-in-side-window          t
  ;; 	  treemacs-eldoc-display                   'simple
  ;; 	  treemacs-file-event-delay                2000
  ;; 	  treemacs-file-extension-regex            treemacs-last-period-regex-value
  ;; 	  treemacs-file-follow-delay               0.2
  ;; 	  treemacs-file-name-transformer           #'identity
  ;; 	  treemacs-follow-after-init               t
  ;; 	  treemacs-expand-after-init               t
  ;; 	  treemacs-find-workspace-method           'find-for-file-or-pick-first
  ;; 	  treemacs-git-command-pipe                ""
  ;; 	  treemacs-goto-tag-strategy               'refetch-index
  ;; 	  treemacs-header-scroll-indicators        '(nil . "^^^^^^")
  ;; 	  treemacs-hide-dot-git-directory          t
  ;; 	  treemacs-indentation                     2
  ;; 	  treemacs-indentation-string              " "
  ;; 	  treemacs-is-never-other-window           nil
  ;; 	  treemacs-max-git-entries                 5000
  ;; 	  treemacs-missing-project-action          'ask
  ;; 	  treemacs-move-forward-on-expand          nil
  ;; 	  treemacs-no-png-images                   nil
  ;; 	  treemacs-no-delete-other-windows         t
  ;; 	  treemacs-project-follow-cleanup          nil
  ;; 	  treemacs-persist-file                    (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
  ;; 	  treemacs-position                        'left
  ;; 	  treemacs-read-string-input               'from-child-frame
  ;; 	  treemacs-recenter-distance               0.1
  ;; 	  treemacs-recenter-after-file-follow      nil
  ;; 	  treemacs-recenter-after-tag-follow       nil
  ;; 	  treemacs-recenter-after-project-jump     'always
  ;; 	  treemacs-recenter-after-project-expand   'on-distance
  ;; 	  treemacs-litter-directories              '("/node_modules" "/.venv" "/.cask")
  ;; 	  treemacs-project-follow-into-home        nil
  ;; 	  treemacs-show-cursor                     nil
  ;; 	  treemacs-show-hidden-files               t
  ;; 	  treemacs-silent-filewatch                nil
  ;; 	  treemacs-silent-refresh                  nil
  ;; 	  treemacs-sorting                         'alphabetic-asc
  ;; 	  treemacs-select-when-already-in-treemacs 'move-back
  ;; 	  treemacs-space-between-root-nodes        t
  ;; 	  treemacs-tag-follow-cleanup              t
  ;; 	  treemacs-tag-follow-delay                1.5
  ;; 	  treemacs-text-scale                      nil
  ;; 	  treemacs-user-mode-line-format           nil
  ;; 	  treemacs-user-header-line-format         nil
  ;; 	  treemacs-wide-toggle-width               70
  ;; 	  treemacs-width                           35
  ;; 	  treemacs-width-increment                 1
  ;; 	  treemacs-width-is-initially-locked       t
  ;; 	  treemacs-workspace-switch-cleanup        nil)

  ;;     ;; The default width and height of the icons is 22 pixels. If you are
  ;;     ;; using a Hi-DPI display, uncomment this to double the icon size.
  ;;     ;;(treemacs-resize-icons 44)

  ;;     (treemacs-follow-mode t)
  ;;     (treemacs-filewatch-mode t)
  ;;     (treemacs-fringe-indicator-mode 'always)
  ;;     (when treemacs-python-executable
  ;;       (treemacs-git-commit-diff-mode t))

  ;;     (pcase (cons (not (null (executable-find "git")))
  ;; 		 (not (null treemacs-python-executable)))
  ;;       (`(t . t)
  ;;        (treemacs-git-mode 'deferred))
  ;;       (`(t . _)
  ;;        (treemacs-git-mode 'simple)))

  ;;     (treemacs-hide-gitignored-files-mode nil))
  ;;   :bind
  ;;   (:map global-map
  ;; 	("M-0"       . treemacs-select-window)
  ;; 	("C-x t 1"   . treemacs-delete-other-windows)
  ;; 	("C-x t t"   . treemacs)
  ;; 	("C-x t d"   . treemacs-select-directory)
  ;; 	("C-x t B"   . treemacs-bookmark)
  ;; 	("C-x t C-t" . treemacs-find-file)
  ;; 	("C-x t M-t" . treemacs-find-tag)))

  ;; (use-package project-treemacs
  ;;   :requires treemacs)

  ;; (use-package treemacs-all-the-icons
  ;;   :config
  ;;   (treemacs-load-theme 'all-the-icons)
  ;;   :requires treemacs)

  ;; (use-package treemacs-magit
  ;;   :requires treemacs
  ;;   :after (treemacs magit))

  (use-package yaml-mode
    :mode
    ("\\.yml\\'" . yaml-mode)
    ("\\.[0-9][0-9][0-9]\\'" . yaml-mode)
    ("\\.[0-9][0-9]\\'" . yaml-mode)
    ("\\.[0-9]\\'" . yaml-mode))

  (use-package
    ini-mode
    :mode
    ("inventory\\'" . ini-mode))
#+end_src

* Themes
#+begin_src emacs-lisp
  (use-package modus-themes
    :demand t
    :custom
    (modus-themes-italic-constructs t)
    (modus-themes-mixed-fonts t)
    (modus-themes-variable-pitch-ui nil)
    (modus-themes-custom-auto-reload t)
    (modus-themes-disable-other-theme t)
    (modus-themes-to-toggle '(modus-operandi-tinted modus-vivendi-tinted))
    (modus-themes-org-blocks 'tinted-background)
    (modus-themes-bold-constructs t)
    :config
    (if ( < (nth 2 (decode-time (current-time))) 20)
	(load-theme 'modus-operandi-tinted t)
      (load-theme 'modus-vivendi-tinted t)
      )
    )

  (use-package ef-themes
    :demand t
    :custom
    (ef-themes-mixed-fonts t)
    (ef-themes-to-toggle '(ef-maris-dark ef-maris-light))
    )
#+end_src
