(deftheme domtheme
  "Created 2023-11-23.")

(defgroup domtheme nil
  "Zeno-theme options."
  :group 'faces)

(defcustom domtheme-enable-italics t
  "Enable italics for functions, comments, directories."
  :type 'boolean
  :group 'domtheme)

(defcustom domtheme-enable-bold t
  "Enable italics for functions, comments, directories."
  :type 'boolean
  :group 'domtheme)

(defcustom domtheme-highlight-color "#7fd6d6"
  "Set a highlight color"
  :type 'string
  :group 'domtheme)

(defcustom domtheme-enable-monaspace-variants t
  "Enable Different Fonts through the Monaspace Font Variants"
  :type 'boolean
  :group 'domtheme)

(let
    (
     (bg                     "#202223") ;; Base Background
     (bg-1                   "#333435") ;; Lighter
     (bg-2                   "#464849") ;; More Lighter
     (bg-3                   "#5B5C5D") ;; Lighter Still
     (bg-4                   "#1A1C1D") ;; Darker
     (bg-5                   "#151617") ;; Darker Still
     (bg-6                   "#0A0B0C") ;; Darkest
     (fg                     "#fde6d7")
     (fg-1                   "#FDE9DB")
     (fg-2                   "#FEEBDF")
     (fg-3                   "#FEF0E7")
     (fg-4                   "#DCC8BB")
     (fg-5                   "#BCAB9F")
     (fg-6                   "#9D8E85")
     (fg-acc                 domtheme-highlight-color)
     (fg-red                 "#dd705a")
     
     (slantType (if domtheme-enable-italics 'italic 'normal))
     (weightType (if domtheme-enable-bold 'bold 'normal))

   )

(custom-theme-set-faces
 'domtheme

;; Default and Interface
`(default ((t (:background ,bg :foreground ,fg))))
`(error ((t (:background ,bg :foreground ,fg-red))))
`(hl-line ((t (:background ,bg-1))))
`(rectangle-preview ((t (:background ,bg-1))))
`(region ((t (:background ,bg-2))))
`(line-number ((t (:foreground ,fg-6))))
`(line-number-current-line ((t (:background ,bg-1 :weight ,weightType))))
`(ffap ((t (:background ,fg-acc :foreground ,bg))))
`(highlight ((t (:background ,bg-2))))
`(lazy-highlight ((t (:background ,fg-acc :foreground ,bg))))
`(minibuffer-prompt ((t (:foreground ,fg-acc))))


;; general Highlighting
`(font-lock-constant-face ((t (:foreground ,fg))))
`(font-lock-type-face ((t (:foreground ,fg-1))))
`(font-lock-comment-face ((t (:foreground ,fg-6 :slant ,slantType))))
`(font-lock-string-face ((t (:foreground ,fg-2 :slant ,slantType))))
`(font-lock-builtin-face ((t (:foreground ,fg-3 :weight ,weightType))))
`(font-lock-keyword-face ((t (:foreground ,fg-3 :weight ,weightType))))
`(font-lock-variable-use-face ((t (:foreground ,fg-3 :slant ,slantType))))
`(font-lock-function-use-face ((t (:foreground ,fg-3 :slant ,slantType))))
`(font-lock-variable-name-face ((t (:foreground ,fg-3 :slant ,slantType))))
`(font-lock-function-name-face ((t (:foreground ,fg-3 :slant ,slantType))))

`(show-paren-match ((t (:background ,fg-acc :foreground ,bg :weight ,weightType))))

;; Orderless
`(orderless-match-face-0 ((t (:foreground ,fg-acc :weight ,weightType))))
`(orderless-match-face-1 ((t (:foreground ,fg-red :weight ,weightType))))
`(orderless-match-face-2 ((t (:foreground ,fg-acc :weight ,weightType :slant ,slantType))))
`(orderless-match-face-3 ((t (:foreground ,fg-red :weight ,weightType :slant ,slantType))))

;; Modeline
`(telephone-line-evil ((t (:background ,bg-1))))
`(telephone-line-evil-normal ((t (:background ,bg-2 :foreground ,fg-acc))))
`(telephone-line-evil-insert ((t (:background ,fg-acc :foreground ,bg))))
`(telephone-line-evil-emacs ((t (:background ,fg-red :foreground ,bg))))
`(mode-line ((t (:background ,bg-1 :foreground ,fg))))

;; Magit
`(magit-branch-local ((t (:foreground ,fg-3 :weight ,weightType :slant ,slantType))))
`(magit-branch-remote ((t (:foreground ,fg-3 :weight ,weightType :slant ,slantType))))
`(magit-section-highlight ((t (:background ,bg-1))))
`(magit-header-line ((t (:foreground ,fg :weight ,weightType))))
`(magit-section-heading ((t (:foreground ,fg :weight ,weightType))))

;; Centaur Tabs
`(tab-bar ((t (:background ,bg-6))))
`(tab-line ((t (:background ,bg-6))))
`(centaur-tabs-selected ((t (:background ,bg :foreground ,fg :weight ,weightType))))
`(centaur-tabs-unselected ((t (:background ,bg-5 :foreground ,fg-6))))
`(centaur-tabs-selected-modified ((t (:background ,bg :foreground ,fg-red :weight ,weightType))))
`(centaur-tabs-unselected-modified ((t (:background ,bg-5 :foreground ,fg-red))))

;; Treemacs
`(treemacs-directory-face ((t (:foreground ,fg))))
`(treemacs-root-face ((t (:foreground ,fg :underline nil :weight ,weightType))))

))

;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'domtheme)
